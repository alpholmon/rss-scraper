# You shouldn't change  name of function or their arguments
# but you can change content of the initial functions.
from argparse import ArgumentParser
from typing import List, Optional, Sequence
import requests
import xml.etree.ElementTree as ET
import json as js

class UnhandledException(Exception):
    pass

def sprintf_result(channel_data, item_data) -> str:
    result = [
        f"Feed:{channel_data['title']}",
        f"Link:{channel_data['link']}",
        f"Description:{channel_data['description']}\n",
        f"Title:{channel_data['title']}",
        f"Published:{channel_data['pubDate']}",
        f"Link:{channel_data['link']}\n"
    ]
    
    items = []
    for item in item_data:
        items.append(f"Title: {item['title']}\n" +\
            f"Author: {item['author']}\n" +\
            f"Published: {item['pubDate']}\n" +\
            f"Link: {item['link']}\n" +\
            f"Category: {item['category']}\n\n" +\
            f"{item['description']}\n\n"
            )
    result.append(" ".join(items))
    return result

def rss_parser(
    xml: str,
    limit: Optional[int] = None,
    json: bool = False,
) -> List[str]:
    """
    RSS parser.

    Args:
        xml: XML document as a string.
        limit: Number of the news to return. if None, returns all news.
        json: If True, format output as JSON.

    Returns:
        List of strings.
        Which then can be printed to stdout or written to file as a separate lines.

    Examples:
        >>> xml = '<rss><channel><title>Some RSS Channel</title><link>https://some.rss.com</link><description>Some RSS Channel</description></channel></rss>'
        >>> rss_parser(xml)
        ["Feed: Some RSS Channel",
        "Link: https://some.rss.com"]
        >>> print("\\n".join(rss_parser(xmls)))
        Feed: Some RSS Channel
        Link: https://some.rss.com
    """
    root = ET.fromstring(xml)   
    channel = root.find('channel')
    items = channel.findall('item')
        
    # Extract channel elements
    channel_data = {
        'title': channel.findtext('title'),
        'link': channel.findtext('link'),
        'description': channel.findtext('description'),
        'lastBuildDate': channel.findtext('lastBuildDate'),
        'pubDate': channel.findtext('pubDate'),
        'language': channel.findtext('language'),
        'categories': [category.text for category in channel.findall('category')],
        'managingEditor': channel.findtext('managingEditor')
    }
        
    # Extract item elements
    item_data = []
    for item in items:
        item_data.append({
            'title': item.findtext('title'),
            'author': item.findtext('author'),
            'pubDate': item.findtext('pubDate'),
            'link': item.findtext('link'),
            'category': item.findtext('category'),
            'description': item.findtext('description')
        })
        
    # Apply limit if provided
    if limit:
        item_data = item_data[:limit]
    # Apply json if provided
    if json:
        output = {
            'title': channel_data['title'],
            'link': channel_data['link'],
            'description': channel_data['description'],
            'items': item_data
        }
        json_output = js.dumps(output, indent=2)
        return json_output
    # return result
    return sprintf_result(channel_data, item_data)

def main(argv: Optional[Sequence] = None):
    """
    The main function of your task.
    """
    parser = ArgumentParser(
        prog="rss_reader",
        description="Pure Python command-line RSS reader.",
    )
    parser.add_argument("source", help="RSS URL", type=str, nargs="?")
    parser.add_argument(
        "--json", help="Print result as JSON in stdout", action="store_true"
    )
    parser.add_argument(
        "--limit", help="Limit news topics if this parameter provided", type=int
    )

    args = parser.parse_args(argv)
    xml = requests.get(args.source).text
    try:
        print("\n".join(rss_parser(xml, args.limit, args.json)))
        return 0
    except Exception as e:
        raise UnhandledException(e)


if __name__ == "__main__":
    main()



